const express = require('express')
const Db = require('./src/db/db');
const sequelize = require('sequelize')

const passport = require('passport');
const Strategy = require('passport-http-bearer').Strategy;
const bcrypt = require('bcryptjs');

const utils = require('./src/utils');
const jwt = utils.jwt;
const ppUtils = utils.passport;


require('dotenv').config();

let db = Db().catch((err)=>{
    console.error(err)
    process.exit(1);
}).then((db)=>{
    passport.use(new Strategy(
        function(token, cb) {
            db.UserModel.findOne({where: {token: token}}).then((user)=>{
                if (!user) { return cb(null, false); }
                else{
                    jwt.checkToken(token).then(()=>{
                        return cb(null, user);
                    }).catch(()=>{
                        return cb(null, false)
                    });
                }
            }).catch((err)=>{
                return cb(err)
            })
        }));

    const app = express()
    app.use(express.json())


    /**
     * @param {string} email
     * @param {string} password
     */
    app.post('/auth', (req,res)=>{
        let error = (err) => {
            res.status(404).json({error: 'Email/Password authentification isn\'t successful'});
        };

        db.UserModel.findOne({
            where: {
                email: req.body.email
            }
        }).then((user)=>{
            if(!user) error('No user found');
            else{
                bcrypt.compare(req.body.password, user.password, (err, match)=>{
                    if(err) error(err);
                    else if(match){
                        jwt.generateToken(user, db.UserModel)
                            .then((token)=>{
                                res.status(200).json({'access_token': token})
                            }).catch(error)
                    }
                })
            }
        });
    })

    app.get('/users', passport.authenticate('bearer', { session: false }), ppUtils.checkRoles('admin'), (req, res)=>{
        db.UserModel.findAll({attributes: ['id', 'email','roles','createdAt','updatedAt']}).then((users)=>{
            res.json(users);
        }).catch(err => {res.json('Gestion des erreurs... ')});
    })

    /**
     * @param {email} email
     * @param {string} password
     * @param {array} roles
     */
    app.post('/user', passport.authenticate('bearer', { session: false }), ppUtils.checkRoles('admin'), (req, res)=>{
        ppUtils.getCryptedPwd(req.body.password).catch(err => {res.json('Gestion des erreurs... ')}).then((hash)=>{
            db.UserModel.create({ email: req.body.email, password: hash, roles : req.body.roles}).then(()=>{
                res.status(201).send();
            }).catch(err => {
                res.json('Gestion des erreurs... '); // TODO
            });
        })
    })

    /**
     * @param {number} id
     */
    app.del('/user/:id([0-9]+)', passport.authenticate('bearer', { session: false }), ppUtils.checkRoles('admin'), (req, res)=>{
        db.UserModel.destroy({where:{id: req.params.id}}).then(()=>{
            res.status(200).send()
        }).catch(err => {
            res.json('Gestion des erreurs... '); // TODO
        });
    })

    /**
     * @param {number} id
     */
    app.get('/user/:id([0-9]+)', passport.authenticate('bearer', { session: false }), ppUtils.checkRoles('admin'), (req, res)=>{
        db.UserModel.findOne({where: {id: req.params.id}}).then((user)=> res.status(user?200:404).json(user)).catch(err => {
            res.json('Gestion des erreurs... '); // TODO
        });
    })

    /**
     *
     */
    app.get('/user/profile', passport.authenticate('bearer', { session: false }), ppUtils.checkRoles('user'), (req, res)=>{
        db.UserModel.findOne({ where : {id: req.user.id}}).then((user)=> res.json(user)).catch(err => {
            res.json('Gestion des erreurs... '); // TODO
        });
    })

    /**
     * @param {Date} startAt
     * @param {Date} endAt
     * @param {[number]} users
     */
    app.post('/contract', passport.authenticate('bearer', { session: false }), ppUtils.checkRoles('admin'), (req,res)=>{
        /**
         * Utilisation d'une transaction pour être sur que le contrat est complet
         */
        db.connection.transaction().catch(err=>console.log(err)).then(transaction => {
            const error = (err)=>{
                transaction.rollback().then(()=>{
                    res.status(400).send()
                }).catch((err)=>{
                    res.status(400).send()
                });
            }

            db.ContractModel.create({startAt: req.body.startAt, endAt: req.body.endAt}, {transaction}).catch(error).then((contract)=>{
                /**
                 * Utilisation d'une reccursive pour etre sur de l'execution complete de chaqune des tasks
                 * Utilisation de la même reccursive pour les 2 types de Many To Many
                 * @param {"user"|"option"}type
                 * @param {[number]} ids
                 * @param {function} cb
                 */
                let rec = (type,ids, cb)=>{
                    if(ids.length === 0) cb()
                    else{
                        switch(type){
                            case 'user':
                                contract.addUser(ids[0], {transaction}).catch(error).then(()=>{
                                    ids.shift();
                                    rec(type, ids, cb);
                                })
                                break;
                            case 'option':
                                contract.addContractOption(ids[0], {transaction}).catch(error).then(()=>{
                                    ids.shift();
                                    rec(type, ids, cb);
                                })
                                break;
                            default:
                                error();
                        }
                    }
                }

                if(req.body.users && req.body.options && typeof req.body.users[0] !== "undefined" && typeof req.body.options[0] !== 'undefined'){
                    if(utils.hasDuplicatedObject(req.body.options)) error('Duplicated option');
                    else{
                        rec('user',req.body.users, ()=>{
                            rec('option', req.body.options, ()=>{
                                transaction.commit().then(()=>{
                                    res.status(201).send();
                                }).catch(error);
                            })
                        })
                    }
                }else error('Missing users or options');
            })
        });
    })

    /**
     * @param {number} id
     */
    app.get('/contract/:id([0-9]+)', passport.authenticate('bearer', { session: false }), ppUtils.checkRoles('user'), (req, res)=>{

        /**
         * Requete sql avec if -> status dynamique
         */
        db.ContractModel.findOne({
            where:{id: req.params.id, },
            attributes:{include:["startAt","endAt", [sequelize.literal("( SELECT IF (CURDATE() < DATE(startAt), 'pending', IF(CURDATE() < DATE(endAt),'active','finished')))"),'status']]},
            include: [
                {model: db.UserModel, attributes: ['id','email',]},
                {model: db.ContractOptionModel, attributes: ["id","desc"]}]
        }).then(contract=>{
            if(!contract) res.status(404).send();
            else contract.hasUser(req.user).then(has=>{
                if(has || req.user.roles.indexOf('admin') !== -1) res.status(200).json(contract);
                else res.status(403).send();
            }).catch(err=>{
                res.json('Gestion des erreurs... '); // TODO
            })
        }).catch(err=>{
            res.json('Gestion des erreurs... '); // TODO
        })
    })

    /**
     *
     */
    app.get('/contracts', passport.authenticate('bearer', { session: false }), ppUtils.checkRoles('user'), (req, res)=>{
        const IS_ADMIN = req.user.roles.indexOf('admin') !== -1;

        /**
         * SI Non admin ne requete que les contrats possédés
         */
        db.ContractModel.findAll({include: [db.ContractOptionModel ,{model : db.UserModel, where: IS_ADMIN?{}:{id: req.user.id}}]}).then(contracts => {
            res.json(contracts);
        }).catch((err)=>{
            res.json('Gestion des erreurs... '); // TODO
        })
    })

    /**
     * @param {number} id
     * @param {Date} terminateAt
     */
    app.patch('/contract/:id([0-9]+)/terminate', passport.authenticate('bearer', { session: false }), ppUtils.checkRoles('user'), (req, res)=>{
        if(new Date(req.body.terminatedAt) < new Date().setUTCHours(0,0,0,0)) res.status(400).send();
         else{
             db.ContractModel.findOne({
                 where:{id: req.params.id, }
             }).then((contract)=>{
                 if(!contract) res.status(404).send();

                 contract.hasUser(req.user).then(has=>{
                     if(has || req.user.roles.indexOf('admin') !== -1) {
                         db.ContractModel.update({terminatedAt: req.body.terminatedAt}, {where: {id: req.params.id}}).then(()=>{
                             res.status(200).send();
                         }).catch(()=>{
                             res.json('Gestion des erreurs... '); // TODO
                         })
                     }
                     else res.status(403).send();
                 }).catch((err)=>{
                     res.json('Gestion des erreurs... '); // TODO
                 })
             })
         }
    })

    app.listen(80, ()=>{
        console.warn('Listening on port 80')
    });
})







