FROM node:14-alpine
WORKDIR /var/www/test
COPY package.json .
COPY package-lock.json .
RUN npm install -g nodemon
COPY . .
CMD npm ci && nodemon index.js