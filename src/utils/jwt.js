const jwt = require('jsonwebtoken')

/**
 *
 * @param user
 * @return {undefined|*}
 */
module.exports = {
    /***
     * Génère un token JWT, et le store dans l'object User pour une authentification ultérieur, 24h de validité
     * @param user
     * @param {UserModel} userModel
     */
    generateToken :  (user, userModel) => {
        return new Promise((resolve, reject) => {
            const token = jwt.sign({data:user}, process.env.NODE_APP_SECRET, {expiresIn: '24h'});

            userModel.update({ token: token }, {
                where: {
                    id: user.id
                }
            }).catch(reject).then(()=>resolve(token))
        })
    },

    /**
     * Vérifie la validité d'un token, resolve si valide, reject si non valide
     * @param token
     * @return {Promise<unknown>}
     */
    checkToken : (token)=>{
        return new Promise((resolve, reject) => {jwt.verify(token, process.env.NODE_APP_SECRET,(err, data) => {
                if(err) reject(err.message);
                else resolve();
            })
        })
    }
}