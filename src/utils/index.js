
module.exports = {
    /**
     * Vérifie la présence d'un clé dupliquée dans un tableau
     * @param array
     * @return {boolean}
     */
    hasDuplicatedObject: function hasDuplicates(array) {
        return (new Set(array)).size !== array.length;
    },

    jwt : require("./jwt"),

    passport: require("./passport")
}