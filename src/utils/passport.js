const bcrypt = require('bcryptjs');

module.exports = {
    /**
     *
     * @param {string} required
     * @return {function(...[*]=)}
     */
    checkRoles : function(required){
        return (req, res, next) => {
            if(req.user.roles.indexOf(required) !== -1) return next();
            else res.status(403).send();
        }
    },

    /**
     *
     * @param {string} clearPwd
     * @return {Promise<string>}
     */
    getCryptedPwd: function (clearPwd) {
        return new Promise((resolve, reject) => {
            bcrypt.genSalt(10, function(err, salt) {
                if(err) reject();
                else{
                    bcrypt.hash(clearPwd, salt, function(err, hash) {
                        if(err) reject();
                        else resolve(hash)
                    });
                }
            });
        })
    }
}