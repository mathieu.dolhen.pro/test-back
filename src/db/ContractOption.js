const { Sequelize, Model, DataTypes } = require("sequelize");

/**
 *
 * @param {Sequelize} sequelize
 * @return Model
 */
module.exports = (sequelize) => {
    return sequelize.define("contractOption", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        desc: {
            type: DataTypes.TEXT,
            allowNull: false
        }
    });
}
