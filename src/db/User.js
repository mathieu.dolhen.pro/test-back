const { Sequelize, Model, DataTypes } = require("sequelize");

/**
 *
 * @param {Sequelize} sequelize
 * @return Model
 */
module.exports = (sequelize) => {
    return sequelize.define("user", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            isEmail: true
        },
        roles: {
            type: DataTypes.STRING,
            defaultValue: '["user"]'
        },
        password: DataTypes.STRING,
        token : DataTypes.TEXT
    });
}
