const { Sequelize } = require('sequelize');
const UserModel = require('./User');
const ContractModel = require('./Contract');
const ContractOptionModel = require('./ContractOption');
const bcrypt = require('bcryptjs')

/**
 *
 * @return {Promise<{
 *     UserModel : UserModel,
 *     ContractModel: ContractModel,
 *     ContractOptionModel: ContractOptionModel
 * }>}
 */
function db(){
    let db = this;

    return new Promise(((resolve, reject) => {
        let connection_try = ()=>{
            return new Promise((resolve, reject)=>{
                const sequelize = new Sequelize('test-db', 'root', 'secret', {
                    host: 'db-service',
                    dialect: 'mysql'/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
                });

                db.UserModel = UserModel(sequelize);
                db.ContractModel = ContractModel(sequelize);
                db.ContractOptionModel = ContractOptionModel(sequelize);

                db.UserModel.belongsToMany(db.ContractModel, {through: 'User_Contract'});
                db.ContractModel.belongsToMany(db.UserModel, {through: 'User_Contract'});
                db.ContractModel.belongsToMany(db.ContractOptionModel, {through: 'Contract_Option'});
                db.ContractOptionModel.belongsToMany(db.ContractModel, {through: 'Contract_Option'});

                sequelize.sync({ alter: true }).then(()=>{
                    sequelize.authenticate().then(()=>{

                        db.UserModel.findAll().then((users)=>{
                            if(users.length === 0) loadFixtures(db);
                        })

                        resolve(sequelize);
                    }).catch((err)=>{
                        console.log(err);
                        reject();
                    })
                }).catch((err)=>{
                    console.log(err);
                    reject()
                });
            });
        }

        connection_try().catch(()=>{
            setTimeout(connection_try, 2000)
        }).then((conn)=>{
            db.connection = conn;

            resolve(db);
        })
    }))

}

/**
 *
 * @param {db} db
 */
function loadFixtures(db){
    console.warn('No users Detected, loading fixtures');

    let users = [];
    for(let i = 1; i < 6; i ++){
        bcrypt.genSalt(10, function(err, salt) {
            bcrypt.hash("secret", salt, function(err, hash) {
                users.push(db.UserModel.create({email: `test+${i}@testing.fr`, password: hash, roles : i === 1?JSON.stringify(['admin', 'user']):undefined}));
            });
        });
    }

    let options = [];
    for(let i = 1; i<6; i++){
        options.push(db.ContractOptionModel.create({desc: `Je suis l'option n°${i}`}));
    }

    Promise.all(options.concat(users)).then((users)=>{
        console.info(`Users created, all passwd are 'secret'`);
    }).catch((err)=>{
        console.error(err);
    })
}


module.exports = db;