const { Sequelize, Model, DataTypes } = require("sequelize");

/**
 *
 * @param {Sequelize} sequelize
 * @return Model
 */
module.exports = (sequelize) => {
    return sequelize.define("contracts", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        startAt: {
            type: DataTypes.DATE,
            allowNull: false
        },
        endAt: {
            type: DataTypes.DATE,
            allowNull: true
        },
        terminatedAt: {
            type: DataTypes.DATE,
            allowNull: true
        }
    });
}
