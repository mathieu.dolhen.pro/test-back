##API faite dans un environnement Docker-compose

_*Installer Docker compose*_ (https://docs.docker.com/get-started/#download-and-install-docker)

Depuis le répertoire de l'app :

    - docker-compose up -d (Attendre le build + lancement de l'app)
    - docker compose logs -f node-service (Affiche les logs du container node)
    
Application : 

    localhost:8080
    
PHPmyAdmin: 
   
    localhost:8081 root:secret
    
*Les utilisateurs par défaut sont :*
 
- test+1@testing.fr > USER + ADMIN
- test+2@testing.fr > USER
- test+3@testing.fr > USER
- test+4@testing.fr > USER
- test+5@testing.fr > USER

__Tous les mots de passes sont `secret`__
    
*Les options par défauts sont :*

- Je suis l'option n°1
- Je suis l'option n°2
- Je suis l'option n°3
- Je suis l'option n°4
- Je suis l'option n°5
    

__Consommation de l'API :__ 

importer le fichier du répertoire de l'app `Test.postman_collection.json` dans postman ou une app compatible.

Si problème du type dans les logs lors du premier démarrage: 

node-service : `MODULE_NOT_FOUND` :

    $ docker-compose stop node-service
    $ docker-compose run node-service sh
    $ npm install
    $ exit
    $ docker compose up -d
    
    