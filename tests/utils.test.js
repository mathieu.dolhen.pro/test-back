const utils = require('../src/utils');
const jwt = require('jsonwebtoken')

test('Array has duplicated entry', ()=>{
    let arr = [1,1,2];

    expect(utils.hasDuplicatedObject(arr)).toBe(true);
})

test('Array has no duplicated entry', ()=>{
    let arr = [1,2,3];

    expect(utils.hasDuplicatedObject(arr)).toBe(false);
})

it('Token validation Expired', ()=>{
    let token = jwt.sign({}, 'app_secret', {expiresIn: 0});
    process.env.NODE_APP_SECRET = 'app_secret'
    expect.assertions(1);

    return utils.jwt.checkToken(token).catch(err => {
        expect(err).toBe('jwt expired');
    })
})


it('Token validation ok', ()=>{
    let token = jwt.sign({}, 'app_secret', {expiresIn: 5});
    process.env.NODE_APP_SECRET = 'app_secret'
    expect.assertions(1);

    return utils.jwt.checkToken(token).then((e)=>{
        expect(e).toBeUndefined();
    })
})